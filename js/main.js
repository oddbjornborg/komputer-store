// -- VERBOSE TOGGLE -- //

var v = true;

// -- DOM -- //

const laptopSelectEl = document.getElementById("laptop-select");
const kompFeaturesEl = document.getElementById("laptop-features-p");
const kompPriceEl = document.getElementById("laptop-price");
const laptopImageEl = document.getElementById("laptop-image");
const laptopTextEl = document.getElementById("laptop-text");

const payTextEl = document.getElementById("pay-text");
const balanceTextEl = document.getElementById("balance-text");
const loanTextEl = document.getElementById("loan-text");
const loanRowEl = document.getElementById("loan-row");

const depositButtonEl = document.getElementById("deposit-button");
const workButtonEl = document.getElementById("work-button");
const loanButtonEl = document.getElementById("loan-button");
const buyButtonEl = document.getElementById("buy-button");
const payLoanButtonEl = document.getElementById("pay-loan-button");

// -- OBJECTS -- //

Bank = {
    balance: 0,
    loan: 0
}

Work = {
    salary: 100,
    pay: 0
}

let gSelectedKomputer = 0;

// -- FUNCTIONS -- //

const createKomputer = (json) => {
    return {
        id: json.id, 
        title: json.title, 
        description: json.description, 
        specs: json.specs, 
        price: json.price, 
        stock: json.stock, 
        active: json.active, 
        image: json.image
    }
}

async function fetchKomputers() {
    const url = "https://noroff-komputer-store-api.herokuapp.com/computers"
    
    await fetch(url)
    .then(response => response.json())
    .then(result => {
        komputers.push(...result);

        laptopSelectEl.innerHTML = "";
        for (const komputer of komputers) {
            laptopSelectEl.insertAdjacentHTML("beforeend", `<option value="${komputer.id}">${komputer.title}</option>`);
        }

        updateLaptop(komputers[0]);
    })
}

function getKomputerById(komputers, id) {
    return komputers.find(function(komputer) {
        return komputer.id === id;
    });
}

const parseCash = amount => amount.toLocaleString("en-GB");

function update() {
    payTextEl.innerText = parseCash(Work.pay);
    balanceTextEl.innerText = parseCash(Bank.balance);
    loanTextEl.innerText = parseCash(Bank.loan);

    if(Bank.loan > 0) {
        loanRowEl.style.visibility = "visible";
        payLoanButtonEl.style.visibility = "visible";
        loanButtonEl.disabled = "true";
    } else {
        loanRowEl.style.visibility = "hidden";
        payLoanButtonEl.style.visibility = "hidden";
        loanButtonEl.removeAttribute("disabled");
    }

    if(Work.pay === 0) {
        depositButtonEl.disabled = "true";
    } else {
        depositButtonEl.removeAttribute("disabled");
    }

}

function updateLaptop(laptop) {

    // Features
    let featureString = "";

    featureString += `<ul>\n`
    for (const line of laptop.specs) {
        featureString += `\r<li>${line}</li>\n`;
    }
    featureString += `</ul>`;

    kompFeaturesEl.innerHTML = featureString;

    // Image
    const src = `"https://noroff-komputer-store-api.herokuapp.com/${laptop.image}"`
    laptopImageEl.innerHTML = `<image class="image" src=${src}></image>`

    // Description
    const laptopText = `
        <h2>${laptop.title}</h2>
        <p>${laptop.description}</p>
    `;
    laptopTextEl.innerHTML = laptopText;

    // Price
    kompPriceEl.innerText = parseCash(laptop.price) + " Kr";
}

// -- EVENT LISTENERS -- //

depositButtonEl.addEventListener("click", () => {
    player.deposit();
    update();
})

workButtonEl.addEventListener("click", () => {
    player.work();
    update();
})

loanButtonEl.addEventListener("click", () => {
    const amount = Number(prompt("Input amount to loan"));

    if(player.canLoan(amount)) {
        player.takeLoan(amount);
        update();
    }
})

laptopSelectEl.addEventListener("change", (event) => {
    const komputerId = event.target.selectedIndex;

    gSelectedKomputer = komputerId;
    updateLaptop(komputers[gSelectedKomputer]);
})

payLoanButtonEl.addEventListener("click", () => {
    player.payLoan();
    update();
})

buyButtonEl.addEventListener("click", () => {
    const komputer = komputers[gSelectedKomputer]

    if(player.canBuy(komputer)) {
        player.purchase(komputer);
        update();
        alert("You bought the komputer! Congratulations!");
    }
    else {
        alert("You can't afford this komputer!");
    }
})

// -- CLASSES -- //

class Salaryman{
    work() {
        let toLog = `Pay: ${Work.pay} -> `

        Work.pay += Work.salary;

        // console.log(toLog + Work.pay);
    }

    canBuy(komputer){
        if(komputer.price > Bank.balance) {
            if(v) console.log("Balance: " + Bank.balance + "\nPrice: " + komputer.price + "\n> You can't afford this komputer!");
            return false;
        }
        else if(komputer.stock === 0) {
            if(v) console.log("Stock: " + komputer.stock + "\n| Komputer is out of stock!");
            return false;
        }

        if(v) console.log("Balance: " + Bank.balance + "\nPrice: " + komputer.price + "\nStock: " + komputer.stock + "\n> Komputer can be bought!");
        return true;
    }

    purchase(komputer){
        const oldBalance = Bank.balance;

        Bank.balance -= komputer.price;
        komputer.stock--;

        let toLog = `Price: ${komputer.price}\nBalance: ${oldBalance} -> ${Bank.balance}\nStock: ${komputer.stock+1} -> ${komputer.stock}`;
        if(v) console.log(toLog + `\n> You bought a ${komputer.title}!`);

    }

    canLoan(amount){
        const maxLoan = 2 * Bank.balance;

        // Check requested loan greater than max loan
        if(amount > maxLoan) {
            console.log("Max loan: " + maxLoan + "\nLoan requested: " + amount + "\n> You can't loan this much!");
            return false;
        }
        // Check unresolved existing loan
        else if(Bank.loan > 0) {
            console.log("Outstanding loan: " + Bank.loan + "\n> You can't take on any more loans!");
            return false;
        }
        
        // Can loan
        console.log("Balance: " + Bank.balance + "\nMax loan: " + maxLoan + "\nLoan requested: " + amount + "\n> You can take this loan!");
        return true;
    }

    takeLoan(amount){
        let oldBalance = Bank.balance;

        Bank.loan += amount;
        Bank.balance += amount;

        if(v) console.log("Loan taken: " + amount + "\nBalance: " + oldBalance + " -> " + Bank.balance + "\n> Your loan was approved!");
    }

    deposit(){
        let toLog = "";
        let oldBalance = Bank.balance;
        let oldLoan = Bank.loan;
        let outstandingSalary = Work.pay; 

        // If player has outstanding loan
        if(Bank.loan > 0){

            const loanToPay = outstandingSalary / 10;

            // If loan greater than downpay
            if(Bank.loan > loanToPay) {
                Bank.loan -= loanToPay;
                outstandingSalary -= loanToPay;

                toLog += `Loan: ${oldLoan} -> ${Bank.loan} (${oldLoan - Bank.loan})\n`
            } 
            // If downpay greater than loan
            else {
                const leftover = loanToPay - Bank.loan;
                Bank.loan = 0;
                outstandingSalary -= (loanToPay - leftover);

                toLog += `Loan: ${oldLoan} -> ${Bank.loan}`
                toLog += "\n> You paid down your loan!\n"
            }
        }
        
        Bank.balance += outstandingSalary;
        Work.pay = 0;

        if(toLog != "") {
            toLog += `Balance: ${oldBalance} -> ${Bank.balance} (${Bank.balance - oldBalance})\n`;
            console.log(toLog);
        }

    }

    payLoan() {
        let oldLoan = Bank.loan;

        if(Bank.loan > Work.pay) {
            Bank.loan -= Work.pay;
            Work.pay = 0;
        }
        else {
            Work.pay -= Bank.loan;
            Bank.loan = 0;
        }

        let toLog = `Loan: ${oldLoan} -> ${Bank.loan}`;
        if(Bank.loan === 0) toLog += `\n> You payed off your loan!`;
        console.log(toLog);
    }
}

const player = new Salaryman();
const komputers = [];

fetchKomputers();
update();