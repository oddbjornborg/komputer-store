# Komputer Store

The purpose of this website is to *very* accurately simulate the everyday hustle of the working man. The site consists of four main elements: The Bank Box, where the user can see their balance and take up loans; the Work Box, where users can work and bank their hard earned money; the Komputer Box, where users can browse an illustrious selection of komputers; and lastly the Komputer Display, where information about the currently selected Komputer is shown. 

The goal of the game(?) is to amass capital, take loans, and buy komputers. In contrast to the real world you can just refresh the site to get out of these loans, so knock yourself out.

## Deployment

The recommended way of viewing the site is to open the repository in vscode, navigate to "index.html" and host the site with Live Server.

## Functionality

### Bank

- Displays user's current bank balance
- Allows for taking loans up to two times the user's current bank balance
    - No interest!

### Work

- User can work to earn pay equal to their set salary
    - Salary is currently set to 100 and can not be modified
- User can deposit their collected pay into the bank
    - If user currently has a loan, 10% of the deposited pay will go to paying down the loan
- User can alternatively choose to pay down their entire loan at once

### Komputer Select

- User can choose what komputer to view by choosing it from the dropdown menu
- Displays the specs of the currently selected komputer

### Komputer Display

- Displays the following information about the currently selected komputer
    - Image
    - Name and description
    - Price
- Allows for purchasing komputers

## How-To

The first step to enjoying the program is to get to work. The work button will amass pay every time it is clicked, and this pay can then be deposited to the bank by clicking the Deposit button.

After you've amassed some capital for yourself, you're ready to take a loan. Interacting with the Loan button in the Bank Box allows you to specify an amount to loan, and this amount can not exceed twice your current bank balance. Loans HAVE to be payed back, so do be careful. 

The end goal is to purchase one or more komputers. The komputers can be browsed by using the dropdown menu in the Komputer Box. To purchase one, click the Buy button in the Komputer Display.

If you make it this far you've done everything there is to do! You are officially a Komputer Store master.

## The technicalities

The main brunt of the site's functionalities are nested within a Salaryman class. This is largely due to the author being used to OOP and not because it's necessarily the best way to structure the program. To make it easier to relate the code to the site while under development, some data was stored away in a couple objects. 

The Work object contains the user's salary and their amassed pay. Similarly there is a Bank object that stores the user's bank balance and current loan. These values could easily have been global constants or part of the Salaryman class, but this structure made it easy - for the author personally - to view the code in the mind's eye, for lack of a sane term. It also allows us not to start every line of code with **this.**, which is great.